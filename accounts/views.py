from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.contrib.auth import login


def signup(request):
    # POST is like add something
    # checking to see if we got a request
    if request.method == "POST":
        # generate a form for them to sign up
        form = UserCreationForm(request.POST)
        # if the form is valid with everythign below.
        if form.is_valid():
            # testing to see if user is adding a username and password
            username = request.POST.get("username")
            password = request.POST.get("password")
            # tying user to the username and password
            user = User.objects.create_user(
                username=username, password=password
            )
            # save the username
            user.save()
            # mush the request of logging in with the user.
            login(request, user)
            # direct back to home
            return redirect("home")
    # if the request was not recieved.
    else:
        # we bring up the form
        form = UserCreationForm()
    # add form to context
    context = {"form": form}
    # render it
    return render(request, "registration/signup.html", context)
