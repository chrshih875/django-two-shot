from django.contrib import admin

# Register your models here.
from receipts.models import Receipt, Account, ExpenseCategory


admin.site.register(Receipt)
admin.site.register(Account)
admin.site.register(ExpenseCategory)
